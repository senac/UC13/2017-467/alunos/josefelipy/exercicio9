
package br.com.senac.ex9;

public class CombustivelGasto {
    
    private final double gasto = 12;
    private final double tempo;
    private final double velocidade;
    private double distancia;
    private double gasolinaGasta;

    public CombustivelGasto(double tempo, double velocidade) {
        this.tempo = tempo;
        this.velocidade = velocidade;
    }

    public double getDistancia() {
        return distancia;
    }

    public void setDistancia(double distancia) {
        this.distancia = distancia;
    }

    public double getGasolinaGasta() {
        return gasolinaGasta;
    }

    public void setGasolinaGasta(double gasolinaGasta) {
        this.gasolinaGasta = gasolinaGasta;
    }

    public double distanciaGasta() {
    
       this.distancia = this.tempo * this.velocidade;
        return this.distancia;
    }

    public double litrosGastos() {
        this.gasolinaGasta = distanciaGasta() / gasto;
        return this.gasolinaGasta;
    
    }
    
    
}
