package br.com.senac.ex9.test;

import br.com.senac.ex9.CombustivelGasto;
import org.junit.Test;
import static org.junit.Assert.*;


public class CombustivelGastoTest {
    
    public CombustivelGastoTest() {
    }
    
    @Test
    public void calculoDaDistancia(){
      CombustivelGasto combustivelGasto = new CombustivelGasto(5, 50);
      
        assertEquals(250 , combustivelGasto.distanciaGasta() , 0.1);
    }
    
    @Test
    
    public void calculoDoGastoEmLitros(){
        CombustivelGasto combustivelGasto = new CombustivelGasto(5, 50);
        double resultado = combustivelGasto.litrosGastos();
        assertEquals(20.83 , resultado , 0.01);
    }
    
}
